// generated from rosidl_generator_c/resource/idl.h.em
// with input from fishbot_interfaces:srv/FishBotConfig.idl
// generated code does not contain a copyright notice

#ifndef FISHBOT_INTERFACES__SRV__FISH_BOT_CONFIG_H_
#define FISHBOT_INTERFACES__SRV__FISH_BOT_CONFIG_H_

#include "fishbot_interfaces/srv/detail/fish_bot_config__struct.h"
#include "fishbot_interfaces/srv/detail/fish_bot_config__functions.h"
#include "fishbot_interfaces/srv/detail/fish_bot_config__type_support.h"

#endif  // FISHBOT_INTERFACES__SRV__FISH_BOT_CONFIG_H_
